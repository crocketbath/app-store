<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
switch (getenv('APPLICATION_ENV')) {
    case 'development': 
        define('DB_NAME', getenv('DB_NAME'));
        define('DB_USER', getenv('DB_USER'));
        define('DB_PASSWORD', getenv('DB_PASSWORD'));
        define('DB_HOST', getenv('DB_HOST'));
        define('WP_SITEURL', getenv('WP_SITEURL')); 
        define('WP_HOME', getenv('WP_HOME'));
        break;
    case 'testing': 
        define('DB_NAME', '');
        define('DB_USER', '');
        define('DB_PASSWORD', '');
        define('DB_HOST', 'localhost');
        define('WP_SITEURL', ''); 
        define('WP_HOME', '');
        break;
    default: 
        define('DB_NAME', 'appstore');
        define('DB_USER', 'appstore');
        define('DB_PASSWORD', 'h5FWT69Kqenyb8S9');
        define('DB_HOST', 'localhost');
        define('WP_SITEURL', 'http://app-store.sanderss.com'); 
        define('WP_HOME', 'http://app-store.sanderss.com');
        break;
}

define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ah<27Zc{b4DqBf)yHE|S|O`C;S]Z9l`]^7?9Ln=1p-@+gw?n&qmYH}!@Z7R0My|&');
define('SECURE_AUTH_KEY',  '-_)l%}d%jw5Gkk5(7g#U@,P>&[u+ig[{<@qGDrCf,Vfns|5mfzH?xo+@^d!f)Ml9');
define('LOGGED_IN_KEY',    '7c(|gu7L`p:-L.M#{8ZeIqe!DEwFzYo>P$b-_Pj%O*DAh&oDRS]?BkS1Lqv3NjZj');
define('NONCE_KEY',        '=|h2Es_F1i;fyOcj3:T)vCvNNj8mZg0s$jrOsl(siy8iyfz;vI4-zi0d :<k6^8r');
define('AUTH_SALT',        'LtZ-~]zFg#j!Nj G)znF,:&#|ZEk-8tBJOx.w40eI&E+EpaSr}w&w1gg)JGPi_`w');
define('SECURE_AUTH_SALT', '4a}R<]7;U`S|F=FL?1^Xe|1jr=+v.pzBG68t[juiz]_V|D+!j| CGP9uNVV`CJgP');
define('LOGGED_IN_SALT',   'F~%T>) (zEF@rj0IPrx U-UHCQixn6.d@AYHzA!D6m|-H2u9R|YIJCr/N5{AVVa*');
define('NONCE_SALT',       'tKF ^WEc|fRIC Ac!ocq[b2ZfviY~Z||7q%)8ZDs29~;%s ,(Ak5-;FlB9C/Bm+h');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
